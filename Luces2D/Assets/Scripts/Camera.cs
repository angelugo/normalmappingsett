using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class Camera : MonoBehaviour
{
    public Transform target; // El objetivo a seguir (por ejemplo, el jugador)
    public float smoothSpeed = 0.125f; // La velocidad de suavizado
    public Vector3 offset; // Desplazamiento de la c�mara respecto al objetivo

    void FixedUpdate()
    {
        if (target == null)
        {
            Debug.LogWarning("El objetivo no est� asignado.");
            return;
        }

        // Posici�n deseada de la c�mara
        Vector3 desiredPosition = target.position + offset;

        // Interpolaci�n lineal entre la posici�n actual y la deseada
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);

        // Asignar la posici�n suavizada a la c�mara
        transform.position = smoothedPosition;
    }
}